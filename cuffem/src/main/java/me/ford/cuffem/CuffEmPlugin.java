package me.ford.cuffem;

import java.util.function.Supplier;

import org.bstats.bukkit.Metrics;
import org.bukkit.plugin.java.JavaPlugin;

import me.ford.cuffem.commands.ClearStorageCommand;
import me.ford.cuffem.commands.CuffCommand;
import me.ford.cuffem.commands.CuffEmCommand;
import me.ford.cuffem.commands.GiveCommand;
import me.ford.cuffem.commands.ReloadCommand;
import me.ford.cuffem.commands.SetCuffItemCommand;
import me.ford.cuffem.commands.SetUncuffSignCommand;
import me.ford.cuffem.commands.UncuffCommand;
import me.ford.cuffem.drops.DespawnDropper;
import me.ford.cuffem.drops.DropTarget;
import me.ford.cuffem.drops.Dropper;
import me.ford.cuffem.drops.GroundDropper;
import me.ford.cuffem.drops.StoreDropper;
import me.ford.cuffem.hooks.VehiclesHook;
import me.ford.cuffem.hooks.QualityArmoryVehiclesHook;
import me.ford.cuffem.listeners.CuffListener;
import me.ford.cuffem.listeners.EscapeListener;
import me.ford.cuffem.listeners.LeaveListener;
import me.ford.cuffem.listeners.MoveListener;
import me.ford.cuffem.listeners.UncuffSignListener;
import me.ford.cuffem.signs.SignIdentifier;
import me.ford.cuffem.utils.CuffIdentifier;
import me.ford.cuffem.utils.Messages;
import me.ford.cuffem.utils.Settings;

public class CuffEmPlugin extends JavaPlugin {
	private final static boolean debug = false;
	private final boolean isBefore1dot14 = isBefore1dot14();
	private Settings settings = null;
	private CuffIdentifier identifier = null;
	private SignIdentifier signIdentifier;
	private Dragger dragger = null;
	private Dropper dropper = null;

	@Override
	public void onEnable() {
		loadPlugin();
		getServer().getPluginManager().registerEvents(new CuffListener(this), this);
		getServer().getPluginManager().registerEvents(new EscapeListener(this), this);
		getServer().getPluginManager().registerEvents(new MoveListener(this), this);
		getServer().getPluginManager().registerEvents(new LeaveListener(this), this);
		if (!isLegacyVersion()) {
			getServer().getPluginManager().registerEvents(new UncuffSignListener(this), this);
		}
		if (settings.getUseQualityArmoryVehicles()) {
			if (getServer().getPluginManager().isPluginEnabled("QualityArmoryVehicles")) {
				getServer().getPluginManager().registerEvents(new QualityArmoryVehiclesHook(this), this);
				getLogger().info("Hooked up to QualityArmoryVehicles.");
			} else {
				getLogger().warning("Could not find QualityArmoryVehicles.");
			}
		}
		if (settings.getUseVehiclesPlugin()) {
			if (getServer().getPluginManager().isPluginEnabled("Vehicles") && getServer().getPluginManager()
					.getPlugin("Vehicles").getClass().getSimpleName().equals("VehiclesMain")) {
				new VehiclesHook(this);
				getLogger().info("Hooked up to Vehicles.");
			}
		}
		getCommand("cuffem").setExecutor(new CuffEmCommand(this));
		getCommand("cuff").setExecutor(new CuffCommand(this));
		getCommand("givecuffs").setExecutor(new GiveCommand(this));
		getCommand("uncuff").setExecutor(new UncuffCommand(this));
		getCommand("setuncuffsign").setExecutor(new SetUncuffSignCommand(this));
		getCommand("setcuffitem").setExecutor(new SetCuffItemCommand(this));
		getCommand("cuffemreload").setExecutor(new ReloadCommand(this));
		getCommand("cuffemclearstoredinv").setExecutor(new ClearStorageCommand(this));
		if (settings.useBstats()) {
			new Metrics(this, 10785);
		}
	}

	public void reload() {
		reloadConfig();
		settings.reload();
		Messages.init(this);
		setupDropper();
	}

	private void loadPlugin() {
		setupConfig();
		settings = new Settings(this);
		Messages.init(this);
		identifier = new CuffIdentifier(this);
		signIdentifier = new SignIdentifier(this);
		dragger = new Dragger(this);
		setupDropper();
	}

	private void setupDropper() {
		DropTarget dt = settings.getDropTarget();
		Supplier<Boolean> shouldRemovedBoundArmor = () -> settings.getRemoveBindedArmor();
		if (dt == DropTarget.GROUND) {
			dropper = new GroundDropper(shouldRemovedBoundArmor);
		} else if (dt == DropTarget.DESPAWN) {
			dropper = new DespawnDropper(shouldRemovedBoundArmor);
		} else if (dt == DropTarget.STORE) {
			dropper = new StoreDropper(shouldRemovedBoundArmor, true);
		} else {
			throw new IllegalStateException("TODO - implement");
		}
	}

	private void setupConfig() {
		saveDefaultConfig();
		getConfig().getDefaults().set("cuff-item", null);
	}

	public void disable() {
		getLogger().info("Disabling plugin");
		getServer().getPluginManager().disablePlugin(this);
	}

	@Override
	public void onDisable() {
		// TODO - do I need something done here?
	}

	public Settings getSettings() {
		return settings;
	}

	public CuffIdentifier getIdenfitier() {
		return identifier;
	}

	public SignIdentifier getSignIdentifier() {
		return signIdentifier;
	}

	public Dragger getDragger() {
		return dragger;
	}

	public Dropper getDropper() {
		return dropper;
	}

	public void debug(String message) {
		if (debug) {
			getLogger().info("DEBUG:" + message);
		}
	}

	public boolean isLegacyVersion() {
		return isBefore1dot14;
	}

	private boolean isBefore1dot14() {
		try {
			Class.forName("org.bukkit.block.TileState");
			return false;
		} catch (ClassNotFoundException e) {
			return true;
		}
	}

}
