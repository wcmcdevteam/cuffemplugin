package me.ford.cuffem.listeners;

import org.bukkit.command.CommandException;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import me.ford.cuffem.CuffEmPlugin;
import me.ford.cuffem.Dragger;
import me.ford.cuffem.Dragger.NotBeingDraggedException;
import me.ford.cuffem.hooks.CitizensHook;
import me.ford.cuffem.utils.Messages;

public class CuffListener implements Listener {
	private final CuffEmPlugin CE;
	
	public CuffListener(CuffEmPlugin plugin) {
		CE = plugin;
	}

	@EventHandler
	public void onCuff(PlayerInteractEntityEvent event) {
		Entity clicked = event.getRightClicked();
		if (clicked instanceof Player && event.getHand() == EquipmentSlot.HAND &&
				(CE.getSettings().allowCuffNPCs() || !CitizensHook.isNPC((Player) clicked))) {
			Player clicker = event.getPlayer();
			Player clickedPlayer = (Player) clicked;
			//CE.debug("Clicking! " + clicker.getName() + "->" + clickedPlayer.getName() + " @ " + this);
			ItemStack clickedWith = clicker.getInventory().getItemInMainHand();
			if (CE.getIdenfitier().areCuffs(clickedWith)) {
				//CE.debug("Are cuffs!");
				boolean clickerCanCuff = clicker.hasPermission("cuffem.cuff");
				boolean clickedIsImmune = clickedPlayer.hasPermission("cuffem.immune");
				if (clickerCanCuff && !clickedIsImmune) {
					//CE.debug("Has perms!");
					Dragger dragManager = CE.getDragger();
					Player alreadyDragging = dragManager.beingDraggedBy(clickedPlayer);
					if (dragManager.isDraggingSpecific(clickedPlayer, clicker)) {
						clicker.sendMessage(Messages.TARGET_DRAGGING_YOU.get());
					} else if (alreadyDragging == null) {
						//CE.debug("Not already dragging!");
						if (dragManager.numberOfDragees(clicker) < CE.getSettings().getMaxCuffedPerPlayer()) {
							//CE.debug("Can drag!");
							// Messages dealt in Dragger
							dragManager.startDragging(clicker, clickedPlayer);
							issueCommands(clicker, clickedPlayer);
						} else {
							clicker.sendMessage(Messages.ALREADY_CUFFING_TOO_MANY.get("\\{nr\\}", String.valueOf(CE.getSettings().getMaxCuffedPerPlayer())));
						}
					} else {
						if (alreadyDragging.equals(clicker)) {
							try {
								// message about stopping dealt in Dragger
								dragManager.stopDragging(clickedPlayer);
							} catch (NotBeingDraggedException e) {
								CE.getLogger().severe("Something went wrong while trying to stop the dragging! " 
										+ alreadyDragging.getName() + " was supposed to have been dragging " + clickedPlayer.getName());
							}
						}
					}
				} else if (!clickerCanCuff) {
					clicker.sendMessage(Messages.NO_PERM_FOR_CUFFS.get());
				} else if (clickedIsImmune) {
					clicker.sendMessage(Messages.PLAYER_IMMUNE.get("\\{player\\}", clickedPlayer.getName()));
				}
			}
		}
	}

	private void issueCommands(Player dragger, Player dragee) {
		for (String cmd : CE.getSettings().getCuffRightClickCommands()) {
			cmd = cmd.replace("{dragger}", dragger.getName()).replace("{dragee}", dragee.getName()).replace("{player}",
					dragee.getName());
			try {
				CE.getServer().dispatchCommand(CE.getServer().getConsoleSender(), cmd);
			} catch (CommandException e) {
				CE.getLogger().warning("Problem running command when cuffing " + dragee.getName() + " by "
						+ dragger.getName() + ":\n" + cmd);
				e.printStackTrace();
			}
		}
	}

	private void onInventory(Cancellable event, Player clickedPlayer) {
		if (!CE.getSettings().stopInteractWhenCuffed()) {
			return;
		}
		Dragger dragManager = CE.getDragger();
		Player alreadyDragging = dragManager.beingDraggedBy(clickedPlayer);
		if (alreadyDragging != null) {
			event.setCancelled(true);
			clickedPlayer.sendMessage(Messages.CUFFED_INVENTORY.get());
		}
	}

	@EventHandler(ignoreCancelled = true)
	public void onInventoryClick(InventoryClickEvent event) {
		onInventory(event, (Player) event.getWhoClicked());
	}

	@EventHandler(ignoreCancelled = true)
	public void onInventoryClick(InventoryDragEvent event) {
		onInventory(event, (Player) event.getWhoClicked());
	}

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent event) {
		if (!CE.getSettings().getStopPvPWhenCuffed()) {
			return;
		}
		Entity damager = event.getDamager();
		if (!(damager instanceof Player)) {
			return;
		}
		if (!CE.getDragger().isBeingDragged((Player) damager)) {
			return;
		}
		Entity theDamaged = event.getEntity();
		if (!(theDamaged instanceof Player)) {
			return;
		}
		damager.sendMessage(Messages.CUFFED_CANNOT_PVP.get());
		event.setCancelled(true);
	}

}
