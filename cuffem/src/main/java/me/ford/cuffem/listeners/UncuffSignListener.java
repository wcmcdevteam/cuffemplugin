package me.ford.cuffem.listeners;

import java.util.Set;

import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

import me.ford.cuffem.CuffEmPlugin;
import me.ford.cuffem.Dragger.NotBeingDraggedException;
import me.ford.cuffem.utils.Messages;

public class UncuffSignListener implements Listener {
    private final CuffEmPlugin plugin;

    public UncuffSignListener(CuffEmPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getHand() != EquipmentSlot.HAND) {
            return;
        }
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }
        Block block = event.getClickedBlock();
        if (!(block.getState() instanceof Sign)) {
            return;
        }
        if (!plugin.getSignIdentifier().isSignBlockMarkedForUncuffing(block)) {
            return;
        }
        Player clicker = event.getPlayer();
        Set<Player> theDragged = plugin.getDragger().isDragging(clicker);
        if (theDragged == null || theDragged.isEmpty()) {
            clicker.sendMessage(Messages.NOT_DRAGGING_ANYONE.get());
            return;
        }
        plugin.getSettings().getUncuffSignInfo().applyCommands(clicker);
        for (Player player : theDragged) {
            try {
                plugin.getDragger().stopDragging(player);
            } catch (NotBeingDraggedException e) {
                plugin.getLogger().warning("Problem uncuffing player while using the uncuff sign");
                e.printStackTrace();
            }
        }
    }
    
}
