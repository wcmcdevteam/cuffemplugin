package me.ford.cuffem.listeners;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.util.Vector;

import me.ford.cuffem.CuffEmPlugin;
import me.ford.cuffem.utils.Utils;

public class MoveListener implements Listener {
	private final CuffEmPlugin CE;
	// consists of dragees
	private Map<Player, Integer> waitingList = new HashMap<Player, Integer>();
	
	public MoveListener(CuffEmPlugin plugin) {
		CE = plugin;
	}
	
	private void handleDragging(Player dragger, Player dragee) {
		//CE.debug(dragee.getName() + " is being dragged!");
		if (!waitingList.containsKey(dragee)) {
			waitingList.put(dragee, 0);
		}
		boolean newWorld = !dragger.getWorld().equals(dragee.getWorld());
		if (newWorld || waitingList.get(dragee)%CE.getSettings().getDragTicks() == 0) {
			// don't do this every tick!
			Location draggerLocation = dragger.getLocation();
			Location drageeLocation = dragee.getLocation();
			if (!draggerLocation.getWorld().equals(drageeLocation.getWorld())) {
				// different world -> tp to dragee to dragger
				dragee.teleport(dragger, TeleportCause.PLUGIN);
			} else if (drageeLocation.distanceSquared(draggerLocation) > CE.getSettings().getDragDistanceSquared()) {
				// teleport closer
				Location locDiff = drageeLocation.clone().subtract(draggerLocation);
				Vector moveBy = new Vector(locDiff.getX(), locDiff.getY(), locDiff.getZ()).normalize().multiply(CE.getSettings().getDragDistance());
				Location newLoc = draggerLocation.add(moveBy);
				try {
					newLoc = Utils.findClosestSuitbaleLocation(newLoc);
				} catch (IllegalArgumentException e) {
					CE.getLogger().warning(Utils.color("&cUnable to find suitable location for TP while cuffing"));
				}
				//CE.debug("Moving closer!" + drageeLocation + "\n->\n" + newLoc + "\n(" + move + ")");
				dragee.teleport(newLoc, TeleportCause.PLUGIN);
			}
		}
		waitingList.put(dragee, waitingList.get(dragee) + 1);
	}
	
	private void handleMove(Player player) {
		handleMoveOrTeleport(player, false);
	}
	
	private void handleTeleport(Player player) {
		handleMoveOrTeleport(player, true);
	}
	
	private void handleMoveOrTeleport(Player player, boolean teleport) {
		Player dragger = CE.getDragger().beingDraggedBy(player);
		Set<Player> dragging = CE.getDragger().isDragging(player);
		if (!teleport && dragger != null) {
			// the dragee is moving / teleporting
			handleDragging(dragger, player);
		} else if (dragging != null){
			// the dragger is moving / teleporting
			for (Player dragee : dragging) {
				handleDragging(player, dragee);
			}
		}
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		handleMove(event.getPlayer());
	}
	
	@EventHandler
	public void onPlayerTeleport(PlayerTeleportEvent event) {
		handleTeleport(event.getPlayer());
	}
	

}
