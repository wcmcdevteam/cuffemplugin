package me.ford.cuffem.listeners;

import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import me.ford.cuffem.CuffEmPlugin;
import me.ford.cuffem.Dragger.NotBeingDraggedException;

public class LeaveListener implements Listener {
	private final CuffEmPlugin CE;
	
	public LeaveListener(CuffEmPlugin plugin) {
		CE = plugin;
	}
	
	private void drageeLeaving(Player dragger, Player dragee) {
		if (dragger != null) {
			try {
				CE.getDragger().stopDragging(dragee, false);
				if (dragger.hasPermission("cuffem.dccommand")) {
					for (String cmd : CE.getSettings().getDrageeLeaveCommands()) {
						cmd = cmd.replace("{player}", dragee.getName());
						cmd = cmd.replace("{from}", dragger.getName());
						CE.getServer().dispatchCommand(CE.getServer().getConsoleSender(), cmd);
					}
				}
			} catch (NotBeingDraggedException e) {
				CE.getLogger().warning("Problem stopping the cuffing when a cuffed player leaves. Left: " + dragee.getName());
			}
		}
	}
	
	private void draggerLeaving(Set<Player> dragees, Player dragger) {
		if (dragees != null && !dragees.isEmpty()) {
			for (Player dragee : dragees) {
				try {
					CE.getDragger().stopDragging(dragee);
				} catch (NotBeingDraggedException e) {
					CE.getLogger().warning("Problem stopping the cuffing when a player (who has cuffed someone) leaves. Left: " + dragger.getName());
				}
			}
		}
	}
	
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent event) {
		Player dragger = CE.getDragger().beingDraggedBy(event.getPlayer());
		drageeLeaving(dragger, event.getPlayer());
		Set<Player> dragees = CE.getDragger().isDragging(event.getPlayer());
		draggerLeaving(dragees, event.getPlayer());
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		if (CE.getDropper().canReturnInventory() && CE.getDropper().hasSavedInventoryFor(player)) {
			CE.getDropper().returnInventory(player);
		}
	}

}
