package me.ford.cuffem.drops.storage.yaml;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import me.ford.cuffem.utils.CustomConfigHandler;

public class YAMLFileManager {
    private static final String FOLDER_NAME = "dropstorage";
    private static final JavaPlugin PLUGIN = JavaPlugin.getProvidingPlugin(YAMLDropStorage.class);
    private static final File FOLDER = new File(PLUGIN.getDataFolder(), FOLDER_NAME);
    static {
        if (!FOLDER.exists()) {
            FOLDER.mkdirs();
        }
    }
    private final Map<CustomConfigHandler, BukkitTask> configsToSave = new HashMap<>();
    private final Map<UUID, CustomConfigHandler> openConfigs = new HashMap<>();

    public boolean hasFileFor(Player player) {
        return hasFileFor(player.getUniqueId());
    }

    public boolean hasFileFor(UUID id) {
        return getFileFor(id).exists();
    }

    private File getFileFor(UUID id) {
        return new File(FOLDER, getFileName(id));
    }

    private String getFileName(Player player) {
        return getFileName(player.getUniqueId());
    }

    private String getFileName(UUID id) {
        return id.toString() + ".yml";
    }

    public CustomConfigHandler getFile(Player player, boolean createNew) {
        if (!createNew && !hasFileFor(player)) {
            throw new IllegalArgumentException("No file for player: " + player.getName());
        }
        CustomConfigHandler handler = openConfigs.get(player.getUniqueId());
        if (handler != null) {
            return handler;
        }
        return new CustomConfigHandler(PLUGIN, FOLDER, getFileName(player), false);
    }

    public void removeFile(Player player) {
        if (!hasFileFor(player)) {
            throw new IllegalArgumentException("No file for player: " + player.getName());
        }
        getFileFor(player.getUniqueId()).delete();
    }

    public void scheduleSave(Player player, CustomConfigHandler config) {
        scheduleSave(player.getUniqueId(), config);
    }

    public void scheduleSave(UUID playerId, CustomConfigHandler config) {
        if (configsToSave.containsKey(config)) {
            PLUGIN.getLogger().finest("[SAVE] Save already scheduled");
            return; // already scheduled
        } else if (openConfigs.containsKey(playerId)) {
            PLUGIN.getLogger().warning("Player UUID found when scheduling a save " +
                    "for their drop config, but the config was not found in the " +
                    "open configs. This would indicate the state of the plugin is " +
                    "unknown. I would expect to see an exceptuion in console some time " +
                    "before this message (and it may be a while before).");
            return; // TODO - warning
        }
        PLUGIN.getLogger().finest("[SAVE] Scheduling save");
        openConfigs.put(playerId, config);
        configsToSave.put(config, PLUGIN.getServer().getScheduler().runTask(PLUGIN, () -> {
            PLUGIN.getLogger().finest("[SAVE] Saveing and removing from sheduled save");
            configsToSave.remove(config);
            openConfigs.remove(playerId);
            config.saveConfig();
        }));
    }

}
