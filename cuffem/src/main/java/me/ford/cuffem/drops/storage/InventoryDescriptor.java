package me.ford.cuffem.drops.storage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class InventoryDescriptor {
    private final ItemStack[] mainContents;

    public InventoryDescriptor(ItemStack[] mainContents) {
        this.mainContents = mainContents;
    }

    /**
     * Distributes all the contents to the player. If something can't go to the
     * correct slot in the inventory, it'll be put elswhere. If something doesn't
     * fit at all, it will be dropped.
     *
     * @param player       Player to distribute items to
     * @param dropConsumer Used to shoe how many items were dropped (only called if
     *                     some were dropped)
     */
    public void distributeAll(Player player, Consumer<Integer> dropConsumer) {
        List<ItemStack> leftovers = distributeItemsTo(player);
        if (!leftovers.isEmpty()) {
            distributeAnywhereAndDropIfNeeded(player, leftovers, dropConsumer);
        }
    }

    private void distributeAnywhereAndDropIfNeeded(Player player, List<ItemStack> items,
            Consumer<Integer> dropConsumer) {
        HashMap<Integer, ItemStack> leftovers = player.getInventory().addItem(items.toArray(new ItemStack[0]));
        if (!leftovers.isEmpty()) {
            Location loc = player.getLocation();
            for (ItemStack item : leftovers.values()) {
                loc.getWorld().dropItemNaturally(loc, item);
            }
            dropConsumer.accept(leftovers.size());
        }
    }

    /**
     * Distribute items to a player and return the leftovers (if present)
     *
     * @param player player the leftovers will be distributed to
     * @return the leftovers (if there are any)
     */
    public List<ItemStack> distributeItemsTo(Player player) {
        List<ItemStack> leftovers = new ArrayList<>();
        for (int slot = 0; slot < mainContents.length; slot++) {
            ItemStack savedStack = mainContents[slot];
            if (savedStack == null) {
                continue; // ignore
            }
            ItemStack cur = player.getInventory().getItem(slot);
            if (cur == null || cur.getType() == Material.AIR) {
                player.getInventory().setItem(slot, savedStack);
            } else {
                leftovers.add(savedStack);
            }
        }
        return leftovers;
    }

    public static InventoryDescriptor fromMaps(Map<Integer, ItemStack> mainContents) {
        ItemStack[] mc = new ItemStack[Collections.max(mainContents.keySet()) + 1];
        for (Map.Entry<Integer, ItemStack> entry : mainContents.entrySet()) {
            mc[entry.getKey()] = entry.getValue();
        }
        return new InventoryDescriptor(mc);
    }

}
