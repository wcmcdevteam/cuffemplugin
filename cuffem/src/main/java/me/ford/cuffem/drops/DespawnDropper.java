package me.ford.cuffem.drops;

import java.util.function.Supplier;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class DespawnDropper extends AbstractDropper {

    public DespawnDropper(Supplier<Boolean> shouldDropBoundArmorGetter) {
        super(shouldDropBoundArmorGetter);
    }

    @Override
    protected void dealWithDroppedItem(Player player, int rawSlot, ItemStack stack) {
        // do nothing - already removed from inventory
    }

    @Override
    public boolean canReturnInventory() {
        return false;
    }

    @Override
    public boolean hasSavedInventoryFor(Player player) {
        return false;
    }

    @Override
    public void returnInventory(Player player) {
        throw new IllegalStateException("The DESPAWN dropper cannot return an inventory");
    }

    @Override
    public void deleteSavedInventory(Player player) {
        throw new IllegalStateException("The DESPAWN dropper does not have saved inventories");
    }

}
