package me.ford.cuffem.drops;

import java.util.function.Supplier;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.ford.cuffem.drops.storage.DropStorage;
import me.ford.cuffem.drops.storage.InventoryDescriptor;
import me.ford.cuffem.drops.storage.yaml.YAMLDropStorage;
import me.ford.cuffem.utils.Messages;

public class StoreDropper extends AbstractDropper {
    private final DropStorage storage;

    public StoreDropper(Supplier<Boolean> shouldDropBoundArmorGetter, boolean useYaml) {
        super(shouldDropBoundArmorGetter);
        if (!useYaml) {
            throw new RuntimeException("Non-yaml storage no defined");
        }
        storage = new YAMLDropStorage();
    }

    @Override
    protected void dealWithDroppedItem(Player player, int rawSlot, ItemStack stack) {
        storage.storePartOfInventory(player, rawSlot, stack);
    }

    @Override
    public boolean canReturnInventory() {
        return true;
    }

    @Override
    public boolean hasSavedInventoryFor(Player player) {
        return storage.hasStoredInventory(player);
    }

    @Override
    public void returnInventory(Player player) {
        InventoryDescriptor desc = storage.getStoredInventory(player, true);
        desc.distributeAll(player, (dropped) -> player
                .sendMessage(Messages.ITEMS_DROPPED_DRAGEE.get("\\{nr\\}", String.valueOf(dropped))));
    }

    @Override
    public void deleteSavedInventory(Player player) {
        storage.clearStoredInventory(player);
    }

}
