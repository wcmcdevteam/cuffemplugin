package me.ford.cuffem.drops.storage.yaml;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import me.ford.cuffem.drops.storage.DropStorage;
import me.ford.cuffem.drops.storage.InventoryDescriptor;
import me.ford.cuffem.utils.CustomConfigHandler;

public class YAMLDropStorage implements DropStorage {
    private static final JavaPlugin PLUGIN = JavaPlugin.getProvidingPlugin(YAMLDropStorage.class);
    private final YAMLFileManager fileManager = new YAMLFileManager();

    @Override
    public void storePartOfInventory(Player player, int rawSlot, ItemStack stack) {
        CustomConfigHandler config = fileManager.getFile(player, true);
        ConfigurationSection invSection = config.getConfig().getConfigurationSection("inventory");
        if (invSection == null) {
            invSection = config.getConfig().createSection("inventory");
        }
        invSection.set(String.valueOf(rawSlot), stack);
        fileManager.scheduleSave(player, config);
    }

    @Override
    public InventoryDescriptor getStoredInventory(Player player, boolean clearStorage) {
        CustomConfigHandler config = fileManager.getFile(player, false);
        ConfigurationSection invSection = config.getConfig().getConfigurationSection("inventory");
        if (clearStorage) {
            clearStoredInventory(player);
        }
        return InventoryDescriptor.fromMaps(convertInventory(invSection)); //, convertEquipment(eqSection));
    }

    private Map<Integer, ItemStack> convertInventory(ConfigurationSection section) {
        Map<Integer, ItemStack> map = new HashMap<>();
        if (section == null) {
            return map;
        }
        for (String key : section.getKeys(false)) {
            Integer nr;
            try {
                nr = Integer.parseInt(key);
            } catch (IllegalArgumentException e) {
                PLUGIN.getLogger().warning("Unable to parse inventory slot " + key + " in " + section.getCurrentPath());
                continue;
            }
            ItemStack item = section.getItemStack(key);
            if (item != null && item.getType() != Material.AIR) {
                map.put(nr, item);
            }
        }
        return map;
    }

    @Override
    public boolean hasStoredInventory(Player player) {
        return fileManager.hasFileFor(player);
    }

    @Override
    public void clearStoredInventory(Player player) {
        fileManager.removeFile(player);
    }

}
