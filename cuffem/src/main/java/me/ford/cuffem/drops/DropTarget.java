package me.ford.cuffem.drops;

public enum DropTarget {
    GROUND, DESPAWN, STORE
}
