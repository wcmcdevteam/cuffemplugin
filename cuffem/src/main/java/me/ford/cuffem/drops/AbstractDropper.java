package me.ford.cuffem.drops;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public abstract class AbstractDropper implements Dropper {
    protected static final List<Integer> ALL_SLOTS = getAllSlotNumbers();
    protected static final Set<EquipmentSlot> ARMOR_SLOTS = EnumSet.of(EquipmentSlot.HEAD, EquipmentSlot.CHEST,
            EquipmentSlot.LEGS, EquipmentSlot.FEET);
    protected static final Map<Integer, EquipmentSlot> RAW_SLOT_TO_EQ_SLOT = getRawSlotToSlotMap();
    protected Supplier<Boolean> shouldDropBoundArmorGetter;

    public AbstractDropper(Supplier<Boolean> shouldDropBoundArmorGetter) {
        this.shouldDropBoundArmorGetter = shouldDropBoundArmorGetter;
    }

    @Override
    public final void dropInventory(Player dragee) {
        PlayerInventory inv = dragee.getInventory();
        for (int rawSlot : ALL_SLOTS) {
            ItemStack stack = inv.getItem(rawSlot);
            if (stack == null || stack.getType().name().contains("AIR")) {
                continue;
            }
            EquipmentSlot eqSlot = RAW_SLOT_TO_EQ_SLOT.getOrDefault(rawSlot, null);
            if (shouldDropItem(dragee, stack, rawSlot, eqSlot)) {
                inv.clear(rawSlot);
                dealWithDroppedItem(dragee, rawSlot, stack);
            }
        }
    }

    protected abstract void dealWithDroppedItem(Player player, int rawSlot, ItemStack stack);

    @Override
    public boolean shouldDropItem(Player player, ItemStack item, int rawSlot, EquipmentSlot slot) {
        if (shouldDropBoundArmorGetter.get()) {
            return true;
        }
        // now return true if is not armor or if is armor AND no curse of binding
        if (!isArmor(item, slot)) {
            return true;
        }
        return item.getEnchantmentLevel(Enchantment.BINDING_CURSE) == 0;
    }

    private boolean isArmor(ItemStack item, EquipmentSlot slot) {
        if (!ARMOR_SLOTS.contains(slot)) {
            return false;
        }
        // in later versions should be possible to check
        // Material#getEquipmentSlot (and whether that is in armor slots)
        // but doesn't seem to be available on my target (1.12)
        if (item == null) {
            return false;
        }
        String itemType = item.getType().name();
        return itemType.endsWith("_HELMET") || itemType.endsWith("_CHESTPLATE") || itemType.endsWith("_LEGGINGS")
                || itemType.endsWith("_BOOTS");
    }

    private static List<Integer> getAllSlotNumbers() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 40; i++) {
            list.add(i);
        }
        return Collections.unmodifiableList(list);
    }

    private static Map<Integer, EquipmentSlot> getRawSlotToSlotMap() {
        Map<Integer, EquipmentSlot> map = new HashMap<>();
        map.put(36, EquipmentSlot.FEET);
        map.put(37, EquipmentSlot.LEGS);
        map.put(38, EquipmentSlot.CHEST);
        map.put(39, EquipmentSlot.HEAD);
        return Collections.unmodifiableMap(map);
    }

}
