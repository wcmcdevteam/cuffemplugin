package me.ford.cuffem.drops;

import org.bukkit.entity.Player;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public interface Dropper {

    boolean shouldDropItem(Player player, ItemStack item, int rawSlot, EquipmentSlot slot);

    void dropInventory(Player player);

    boolean canReturnInventory();

    boolean hasSavedInventoryFor(Player player);

    void returnInventory(Player player);

    void deleteSavedInventory(Player player);

}
