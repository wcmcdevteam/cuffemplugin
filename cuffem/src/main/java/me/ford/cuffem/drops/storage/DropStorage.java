package me.ford.cuffem.drops.storage;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface DropStorage {

    void storePartOfInventory(Player player, int rawSlot, ItemStack stack);

    InventoryDescriptor getStoredInventory(Player player, boolean clearStorage);

    void clearStoredInventory(Player player);

    boolean hasStoredInventory(Player player);

}
