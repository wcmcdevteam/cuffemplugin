package me.ford.cuffem.signs;

import org.bukkit.NamespacedKey;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.TileState;
import org.bukkit.persistence.PersistentDataType;

import me.ford.cuffem.CuffEmPlugin;

public class SignIdentifier {
    private static final String SIGN_KEY_NAME = "UncuffSign";
    private final NamespacedKey signKey;

    public SignIdentifier(CuffEmPlugin plugin) {
        signKey = new NamespacedKey(plugin, SIGN_KEY_NAME);
    }

    public void markSignBlockForUncuffing(Block block) throws IllegalArgumentException {
        BlockState state = block.getState();
        if (!(state instanceof Sign)) {
            throw new IllegalArgumentException("Block is not a sign - cannnot sign it");
        }
        TileState sign = (TileState) state;
        sign.getPersistentDataContainer().set(signKey, PersistentDataType.STRING, SIGN_KEY_NAME); // TODO - some data? commands?
        state.update();
    }

    public boolean isSignBlockMarkedForUncuffing(Block block)  throws IllegalArgumentException {
        BlockState state = block.getState();
        if (!(state instanceof Sign)) {
            throw new IllegalArgumentException("Block is not a sign - cannnot sign it");
        }
        TileState sign = (TileState) state;
        return sign.getPersistentDataContainer().has(signKey, PersistentDataType.STRING);
    }

}
