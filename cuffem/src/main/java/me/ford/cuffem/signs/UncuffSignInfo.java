package me.ford.cuffem.signs;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.entity.Player;

import me.ford.cuffem.CuffEmPlugin;

public class UncuffSignInfo {
    private final CuffEmPlugin plugin;
    private final List<String> commands = new ArrayList<>();

    public UncuffSignInfo(CuffEmPlugin plugin, List<String> commands) {
        this.plugin = plugin;
        this.commands.addAll(commands);
    }

    public List<String> getCommands() {
        return new ArrayList<>(commands);
    }

    public void applyCommands(Player clicker) {
        Set<Player> theDragged = plugin.getDragger().isDragging(clicker);
        if (theDragged == null || theDragged.isEmpty()) {
            throw new IllegalArgumentException("Player is not dragging anyone");
        }
        for (Player cuffed : theDragged) {
            for (String cmd : commands) {
                cmd = cmd.replace("{cuffed}", cuffed.getName());
                try {
                    plugin.getServer().dispatchCommand(clicker, cmd);
                } catch (Exception e) {
                    plugin.getLogger().warning("Problem executing command on sign click: " + cmd);
                    e.printStackTrace();
                }
            }
        }
    }

    
}
