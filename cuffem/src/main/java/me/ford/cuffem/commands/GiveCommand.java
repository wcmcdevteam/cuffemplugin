package me.ford.cuffem.commands;

import java.util.Collections;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import me.ford.cuffem.CuffEmPlugin;
import me.ford.cuffem.utils.Messages;

public class GiveCommand implements TabExecutor {
    private final CuffEmPlugin plugin;

    public GiveCommand(CuffEmPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 1) {
            return null;
        }
        return Collections.emptyList();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 1) {
            return false;
        }
        @SuppressWarnings("deprecation")
        Player player = plugin.getServer().getPlayer(args[0]);
        if (player == null) {
            sender.sendMessage(Messages.PLAYER_NOT_FOUND.get("\\{player\\}", args[0]));
            return true;
        }
        player.getInventory().addItem(plugin.getIdenfitier().getCuffs());
        sender.sendMessage(Messages.GAVE_PLAYER_CUFFS.get("\\{player\\}", player.getName()));
        plugin.getLogger().info(
                Messages.LOG_PLAYER_GOT_CUFFS.get("\\{player\\}", player.getName(), "\\{from\\}", sender.getName()));
        player.sendMessage(Messages.RECEIVED_CUFFS.get());
        return true;
    }

}
