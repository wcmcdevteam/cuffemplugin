package me.ford.cuffem.commands;

import java.util.Collections;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import me.ford.cuffem.CuffEmPlugin;
import me.ford.cuffem.Dragger.NotBeingDraggedException;
import me.ford.cuffem.utils.Messages;

public class UncuffCommand implements TabExecutor {
    private final CuffEmPlugin plugin;

    public UncuffCommand(CuffEmPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 1) {
            return null;
        }
        return Collections.emptyList();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length < 1) {
			return false;
		}
		@SuppressWarnings("deprecation")
		Player player = plugin.getServer().getPlayer(args[0]);
		if (player == null) {
			sender.sendMessage(Messages.PLAYER_NOT_FOUND.get("\\{player\\}", args[0]));
			return true;
		}
		if (!plugin.getDragger().isBeingDragged(player)) {
			sender.sendMessage(Messages.PLAYER_NOT_CUFFED.get("\\{player\\}", player.getName()));
			return true;
		}
		if (!sender.hasPermission("cuffem.uncuff.other")) {
			Player dragger = plugin.getDragger().beingDraggedBy(player);
			if (dragger != sender) {
				sender.sendMessage(Messages.PLAYER_NOT_CUFFED_BY_YOU.get("\\{player\\}", player.getName()));
				return true;
			}
		}
		try {
			plugin.getDragger().stopDragging(player);
		} catch (NotBeingDraggedException e) { // should not happen
			e.printStackTrace();
		}
        sender.sendMessage(Messages.UNCUFFED_PLAYER.get("\\{player\\}", player.getName()));
        return true;
    }
    
}
