package me.ford.cuffem.commands;

import java.util.Collections;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import me.ford.cuffem.CuffEmPlugin;

public class CuffEmCommand implements TabExecutor {
    private final CuffEmPlugin plugin;

    public CuffEmCommand(CuffEmPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        return Collections.emptyList();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sender.sendMessage(
                ChatColor.BLUE + "CuffEm plugin version: " + ChatColor.AQUA + plugin.getDescription().getVersion());
        return true;
    }

}
