package me.ford.cuffem.commands;

import java.util.Collections;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import me.ford.cuffem.CuffEmPlugin;
import me.ford.cuffem.utils.Messages;

public class ReloadCommand implements TabExecutor {
    private final CuffEmPlugin plugin;

    public ReloadCommand(CuffEmPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        return Collections.emptyList();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        plugin.reload();
        sender.sendMessage(Messages.RELOADED.get());
        return true;
    }

}
