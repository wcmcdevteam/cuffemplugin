package me.ford.cuffem.commands;

import java.util.Collections;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import me.ford.cuffem.CuffEmPlugin;
import me.ford.cuffem.utils.Messages;

public class ClearStorageCommand implements TabExecutor {
    private final CuffEmPlugin plugin;

    public ClearStorageCommand(CuffEmPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 1) {
            return null; // player names
        }
        return Collections.emptyList();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 1) {
            return false;
        }
        @SuppressWarnings("deprecation")
        Player target = plugin.getServer().getPlayer(args[0]); // getPlayer(String) is deprecated
        if (!plugin.getSettings().getDropItemsWhenCuffed()) {
            sender.sendMessage(Messages.ITEMS_NOT_DROPPING_UPON_CUFF.get());
            return true;
        }
        if (!plugin.getDropper().canReturnInventory()) {
            sender.sendMessage(Messages.DROPPER_STRAT_NO_STORE.get());
            return true;
        }
        if (!plugin.getDropper().hasSavedInventoryFor(target)) {
            sender.sendMessage(Messages.DROPPER_NO_STORED_INVENTORY.get("\\{player\\}", target.getName()));
            return true;
        }
        plugin.getDropper().deleteSavedInventory(target);
        sender.sendMessage(Messages.DROPPER_CLEARED_STORED_INVENTORY.get("\\{player\\}", target.getName()));
        return true;
    }

}
