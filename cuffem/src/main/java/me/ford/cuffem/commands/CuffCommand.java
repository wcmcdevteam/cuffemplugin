package me.ford.cuffem.commands;

import java.util.Collections;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import me.ford.cuffem.CuffEmPlugin;
import me.ford.cuffem.Dragger;
import me.ford.cuffem.hooks.CitizensHook;
import me.ford.cuffem.utils.Messages;

public class CuffCommand implements TabExecutor {
    private final CuffEmPlugin plugin;

    public CuffCommand(CuffEmPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 1) {
            return null;
        }
        return Collections.emptyList();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Need a player");
            return true;
        }
        if (args.length < 1) {
            return false;
        }
        Player cuffer = (Player) sender;
        @SuppressWarnings("deprecation")
        Player target = plugin.getServer().getPlayer(args[0]);
        if (target == null || (!plugin.getSettings().allowCuffNPCs() && CitizensHook.isNPC(target))) {
            cuffer.sendMessage(Messages.PLAYER_NOT_FOUND.get("\\{player\\}", args[0]));
            return true;
        }
        if (cuffer == target) {
            cuffer.sendMessage(Messages.CANNOT_CUFF_SELF.get());
            return true;
        }
        if (target.hasPermission("cuffem.immune")) {
            cuffer.sendMessage(Messages.PLAYER_IMMUNE.get("\\{player\\}", args[0]));
            return true;
        }
        boolean onlySameWorld = plugin.getSettings().cuffCommandWithinSameWorld();
        boolean isSameWorld = target.getWorld() == cuffer.getWorld();
        if (onlySameWorld && !isSameWorld) {
            cuffer.sendMessage(Messages.TARGET_IN_WRONG_WORLD.get());
            return true;
        }
        double dist2 = plugin.getSettings().cuffCommandRadiusSquared();
        if (onlySameWorld && isSameWorld && dist2 > 0
                && target.getLocation().distanceSquared(cuffer.getLocation()) > dist2) {
            cuffer.sendMessage(Messages.TARGET_TOO_FAR.get());
            return true;
        }
        Dragger dragManager = plugin.getDragger();
        Player prevDragger = dragManager.beingDraggedBy(target);
        if (prevDragger == null) {
            if (dragManager.numberOfDragees(cuffer) < plugin.getSettings().getMaxCuffedPerPlayer()) {
                dragManager.startDragging(cuffer, target);
            } else {
                cuffer.sendMessage(Messages.ALREADY_CUFFING_TOO_MANY.get("\\{nr\\}",
                        String.valueOf(plugin.getSettings().getMaxCuffedPerPlayer())));
            }
        } else {
            if (prevDragger.equals(cuffer)) {
                sender.sendMessage(Messages.ALREADY_CUFFING.get("\\{player\\}", args[0]));
            } else {
                sender.sendMessage(Messages.PLAYER_NOT_CUFFED_BY_YOU.get("\\{player\\}", target.getName()));
            }
        }

        return true;
    }

}
