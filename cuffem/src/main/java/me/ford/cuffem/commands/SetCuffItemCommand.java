package me.ford.cuffem.commands;

import java.util.Collections;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.ford.cuffem.CuffEmPlugin;
import me.ford.cuffem.utils.Messages;

public class SetCuffItemCommand implements TabExecutor {
    private final CuffEmPlugin plugin;

    public SetCuffItemCommand(CuffEmPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        return Collections.emptyList();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Need a player");
            return true;
        }
        ItemStack item = ((Player) sender).getInventory().getItemInMainHand();
        if (item == null || item.getType() == Material.AIR) {
            sender.sendMessage(Messages.NO_ITEM_TO_SET.get());
            return true;
        }
        plugin.getSettings().setCuffItem(item);
        sender.sendMessage(Messages.SET_CUFF_ITEM.get());
        return true;
    }

}
