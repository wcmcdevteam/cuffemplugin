package me.ford.cuffem.commands;

import java.util.Collections;
import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import me.ford.cuffem.CuffEmPlugin;
import me.ford.cuffem.signs.SignIdentifier;
import me.ford.cuffem.utils.Messages;

public class SetUncuffSignCommand implements TabExecutor {
    private static final int MAX_DISTANCE = 5;
    private final CuffEmPlugin plugin;

    public SetUncuffSignCommand(CuffEmPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        return Collections.emptyList();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Need a player to execute this command");
            return true;
        }
        if (plugin.isLegacyVersion()) { // TileState / persistence API did not exist before 1.14(.1)
            sender.sendMessage("Unfortunately this feature does not work on versions before 1.14.1");
            return true;
        }
        Player player = (Player) sender;
        Block block = player.getTargetBlock(null, MAX_DISTANCE);
        if (block == null || !(block.getState() instanceof Sign)) {
            player.sendMessage(Messages.NOT_LOOKING_AT_SIGN.get());
            return true;
        }

        SignIdentifier identifier = plugin.getSignIdentifier();
        if (identifier.isSignBlockMarkedForUncuffing(block)) {
            sender.sendMessage(Messages.SIGN_ALREADY_MARKED.get());
            return true;
        }
        identifier.markSignBlockForUncuffing(block);
        sender.sendMessage(Messages.MARKED_SIGN_AS_UNCUFF_SIGN.get());
        return true;
    }
    
}
