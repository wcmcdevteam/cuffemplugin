package me.ford.cuffem.utils;

import org.bukkit.inventory.ItemStack;

import me.ford.v1_12_Tagger;
import me.ford.v1_13_Tagger;
import me.ford.cuffem.CuffEmPlugin;

public class CuffIdentifier {
	private static boolean hasWarnedOnceRead = false;
	private static boolean hasWarnedOnceWrite = false;
	private final CuffEmPlugin CE;
	private final String version;
	private ItemStack noTagCuffs;
	private ItemStack cuffs;
	private final String metTag;
	
	public CuffIdentifier(CuffEmPlugin plugin) {
		CE = plugin;
		version = CE.getServer().getVersion();
		metTag = CE.getSettings().getCuffTag();
		cuffs = _getCuffs();
	}
	
	private ItemStack _getCuffs() {
		ItemStack item = CE.getSettings().getCuffItem();
		noTagCuffs = item.clone();
		
		item = addTag(item);
		
		return item;
	}

	/**
	 * Used for when setting new cuffs
	 * @param cuffs
	 */
	void setCuffs(ItemStack cuffs) {
		this.noTagCuffs = cuffs.clone();
		this.cuffs = addTag(cuffs);
	}
	
	public ItemStack getCuffs() {
		return CE.getSettings().allowCuffsWithoutTag() ? this.noTagCuffs : this.cuffs;
	}

	public boolean areCuffsWithoutTag(ItemStack item) {
		if (item == null) {
			return false;
		}
		return noTagCuffs.isSimilar(item);
	}
	
	public boolean areCuffs(ItemStack item) {
		if (CE.getSettings().allowCuffsWithoutTag()) {
			return areCuffsWithoutTag(item) || checkTag(item);
		}
		//CE.debug("Checking:" + item);
		if (item.getType() == cuffs.getType()) {
			//CE.debug("Is correct type");
			// check NBT
			return checkTag(item);
		}
		return false;
	}
	
	private ItemStack addTag(ItemStack item) {
		if (version.contains("1.12")) {
			return v1_12_Tagger.tag(item, metTag, metTag);
		} else if (version.contains("1.13")) {
			return v1_13_Tagger.tag(item, metTag, metTag);
		} else if (version.contains("1.14") || version.contains("1.15") || version.contains("1.16")) {
			return GeneralTagger.tag(CE, item, metTag, metTag);
		} else {
			if (!hasWarnedOnceWrite) {
				CE.getLogger().warning("Could not find suitable version to tag an item. Trying general tagger (1.14.3+) anyway");
				hasWarnedOnceWrite = true;
			}
			return GeneralTagger.tag(CE, item, metTag, metTag);
		}
	}
	
	private boolean checkTag(ItemStack item) {
		if (version.contains("1.12")) {
			return v1_12_Tagger.checkTag(item, metTag, metTag);
		} else if (version.contains("1.13")) {
			return v1_13_Tagger.checkTag(item, metTag, metTag);
		} else if (version.contains("1.14") || version.contains("1.15") || version.contains("1.16")) {
			return GeneralTagger.checkTag(CE, item, metTag, metTag);
		} else {
			if (!hasWarnedOnceRead) {
				CE.getLogger().warning("Could not find suitable version to read item tag. Trying general tagger (1.14.3+) anyway");
				hasWarnedOnceRead = true;
			}
			return GeneralTagger.checkTag(CE, item, metTag, metTag);
		}
	}
}
