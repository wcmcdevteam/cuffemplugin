package me.ford.cuffem.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

public class Utils {
	public static String color(String msg) {
		return ChatColor.translateAlternateColorCodes('&', msg);
	}
	
	public static Location findClosestSuitbaleLocation(Location startLoc) {
		Location endLoc = startLoc.clone();
		for (Vector vec : toCheck) {
			if (isSafe(endLoc, vec)) {
				return endLoc.add(vec);
			}
		}
		throw new IllegalArgumentException("Could not find a safe location!");
		//return startLoc;
	}
	
	public static boolean isSafe(Location loc, Vector add) {
		World world = loc.getWorld();
		return !world.getBlockAt(loc.getBlockX() + add.getBlockX(), 
									loc.getBlockY() + add.getBlockY(), 
									loc.getBlockZ() + add.getBlockZ()).getType().isSolid() && 
				!world.getBlockAt(loc.getBlockX() + add.getBlockX(), 
									loc.getBlockY() + add.getBlockY() + 1, 
									loc.getBlockZ() + add.getBlockZ()).getType().isSolid() && 
				world.getBlockAt(loc.getBlockX() + add.getBlockX(), 
						loc.getBlockY() + add.getBlockY() - 1, 
						loc.getBlockZ() + add.getBlockZ()).getType().isSolid() ;
	}

	@SuppressWarnings("serial")
	private static final List<Vector> toCheck = new ArrayList<Vector>() {{
		// block itself (1)
		add(new Vector( 0, 0, 0));
		// immediate neighbours (6)
		add(new Vector( 0, 0, 1));
		add(new Vector( 0, 1, 0));
		add(new Vector( 1, 0, 0));
		add(new Vector( 0, 0,-1));
		add(new Vector( 0,-1, 0));
		add(new Vector(-1, 0, 0));
		// 1d diagonals (12)
		add(new Vector( 1, 1, 0));
		add(new Vector( 1,-1, 0));
		add(new Vector(-1, 1, 0));
		add(new Vector(-1,-1, 0));
		
		add(new Vector( 1, 0, 1));
		add(new Vector( 1, 0,-1));
		add(new Vector(-1, 0, 1));
		add(new Vector(-1, 0,-1));
		
		add(new Vector( 0, 1, 1));
		add(new Vector( 0, 1,-1));
		add(new Vector( 0,-1, 1));
		add(new Vector( 0,-1,-1));
		// 2d diagonals (8)
		add(new Vector( 1, 1, 1));
		add(new Vector( 1, 1,-1));
		add(new Vector( 1,-1, 1));
		add(new Vector(-1, 1, 1));
		add(new Vector( 1,-1,-1));
		add(new Vector(-1, 1,-1));
		add(new Vector(-1,-1, 1));
		add(new Vector(-1,-1,-1));
		
		// 2nd immediate neighrbours (6)
		add(new Vector( 0, 0, 2));
		add(new Vector( 0, 2, 0));
		add(new Vector( 2, 0, 0));
		add(new Vector( 0, 0,-2));
		add(new Vector( 0,-2, 0));
		add(new Vector(-2, 0, 0));
		// 2nd 1d almost diagonals (24)
		add(new Vector( 1, 2, 0));
		add(new Vector( 1,-2, 0));
		add(new Vector(-1, 2, 0));
		add(new Vector(-1,-2, 0));
		add(new Vector( 2, 1, 0));
		add(new Vector( 2,-1, 0));
		add(new Vector(-2, 1, 0));
		add(new Vector(-2,-1, 0));
		
		add(new Vector( 1, 0, 2));
		add(new Vector( 1, 0,-2));
		add(new Vector(-1, 0, 2));
		add(new Vector(-1, 0,-2));
		add(new Vector( 2, 0, 1));
		add(new Vector( 2, 0,-1));
		add(new Vector(-2, 0, 1));
		add(new Vector(-2, 0,-1));
		
		add(new Vector( 0, 1, 2));
		add(new Vector( 0, 1,-2));
		add(new Vector( 0,-1, 2));
		add(new Vector( 0,-1,-2));
		add(new Vector( 0, 2, 1));
		add(new Vector( 0, 2,-1));
		add(new Vector( 0,-2, 1));
		add(new Vector( 0,-2,-1));
		// 2nd 2d almsot diagonals (24)
		add(new Vector( 2, 1, 1));
		add(new Vector( 2, 1,-1));
		add(new Vector( 2,-1, 1));
		add(new Vector( 2,-1,-1));
		
		add(new Vector(-2, 1, 1));
		add(new Vector(-2, 1,-1));
		add(new Vector(-2,-1, 1));
		add(new Vector(-2,-1,-1));

		add(new Vector( 1, 2, 1));
		add(new Vector( 1, 2,-1));
		add(new Vector(-1, 2, 1));
		add(new Vector(-1, 2,-1));

		add(new Vector( 1,-2, 1));
		add(new Vector( 1,-2,-1));
		add(new Vector(-1,-2, 1));
		add(new Vector(-1,-2,-1));

		add(new Vector( 1, 1, 2));
		add(new Vector( 1,-1, 2));
		add(new Vector(-1, 1, 2));
		add(new Vector(-1,-1, 2));

		add(new Vector( 1, 1,-2));
		add(new Vector( 1,-1,-2));
		add(new Vector(-1, 1,-2));
		add(new Vector(-1,-1,-2));
		// 2nd 1d diagonals (12)
		add(new Vector( 2, 2, 0));
		add(new Vector( 2,-2, 0));
		add(new Vector(-2, 2, 0));
		add(new Vector(-2,-2, 0));
		
		add(new Vector( 2, 0, 2));
		add(new Vector( 2, 0,-2));
		add(new Vector(-2, 0, 2));
		add(new Vector(-2, 0,-2));
		
		add(new Vector( 0, 2, 2));
		add(new Vector( 0, 2,-2));
		add(new Vector( 0,-2, 2));
		add(new Vector( 0,-2,-2));
		// 2nd 2d almost (closer) diagonals (24)
		add(new Vector( 1, 2, 2));
		add(new Vector( 2, 1, 2));
		add(new Vector( 2, 2, 1));

		add(new Vector(-1, 2, 2));
		add(new Vector(-2, 1, 2));
		add(new Vector(-2, 2, 1));

		add(new Vector( 1,-2, 2));
		add(new Vector( 2,-1, 2));
		add(new Vector( 2,-2, 1));

		add(new Vector( 1, 2,-2));
		add(new Vector( 2, 1,-2));
		add(new Vector( 2, 2,-1));

		add(new Vector( 1,-2,-2));
		add(new Vector( 2,-1,-2));
		add(new Vector( 2,-2,-1));

		add(new Vector(-1, 2,-2));
		add(new Vector(-2, 1,-2));
		add(new Vector(-2, 2,-1));

		add(new Vector(-1,-2, 2));
		add(new Vector(-2,-1, 2));
		add(new Vector(-2,-2, 1));
		
		add(new Vector(-1,-2,-2));
		add(new Vector(-2,-1,-2));
		add(new Vector(-2,-2,-1));
	}};
}
