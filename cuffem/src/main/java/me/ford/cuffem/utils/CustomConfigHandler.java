package me.ford.cuffem.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * CustomConfig
 */
public class CustomConfigHandler {
    private final JavaPlugin plugin;
    private final File folder;
    private final String fileName;
    private final boolean noSave;
    private FileConfiguration customConfig = null;
    private File customConfigFile = null;

    public CustomConfigHandler(JavaPlugin plugin, String name) {
        this(plugin, null, name);
    }

    public CustomConfigHandler(JavaPlugin plugin, File folder, String name) {
        this(plugin, folder, name, false);
    }

    public CustomConfigHandler(JavaPlugin plugin, File folder, String name, boolean noSave) {
        if (folder == null) {
            this.folder = plugin.getDataFolder();
        } else {
            this.folder = folder;
        }
        this.plugin = plugin;
        this.fileName = name;
        this.noSave = noSave;
        saveDefaultConfig();
        reloadConfig();
    }

    public boolean reloadConfig() {
        if (customConfigFile == null) {
            customConfigFile = new File(this.folder, fileName);
        }
        customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

        // Look for defaults in the jar
        Reader defConfigStream = null;
        InputStream resource = plugin.getResource(fileName);
        if (resource != null) {
            try {
                defConfigStream = new InputStreamReader(resource, "UTF8");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }
        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            customConfig.setDefaults(defConfig);
        }
        if (customConfig.getKeys(true).isEmpty()) {
            if (!noSave)
                saveConfig();
            return false;
        } else {
            return true;
        }
    }

    public File getFile() {
        return customConfigFile;
    }

    public FileConfiguration getConfig() {
        return customConfig;
    }

    public void saveConfig() {
        if (customConfig == null || customConfigFile == null) {
            return;
        }
        try {
            getConfig().save(customConfigFile);
        } catch (IOException ex) {
            plugin.getLogger().log(Level.SEVERE, "Could not save config to " + customConfigFile, ex);
        }
    }

    public void saveDefaultConfig() {
        if (customConfigFile == null) {
            customConfigFile = new File(folder, fileName);
        }
        if (!customConfigFile.exists() && plugin.getResource(fileName) != null) {
            plugin.saveResource(fileName, false);
        }
    }

}
