package me.ford.cuffem.utils;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.ford.cuffem.CuffEmPlugin;
import me.ford.cuffem.drops.DropTarget;
import me.ford.cuffem.signs.UncuffSignInfo;

public class Settings {
	private final static String cuffTag = "CuffEmCuffs";
	private final CuffEmPlugin CE;
	private FileConfiguration config;

	public Settings(CuffEmPlugin plugin) {
		CE = plugin;
		config = CE.getConfig();
		loadSettings();
	}

	public void loadSettings() {
		// loading all the variables from the config
		config = CE.getConfig();

		cuffType = _getCuffType();
		cuffName = _getCuffName();
		cuffItem = _getCuffItem();
		maxCuffedPerPlayer = _getMaxCuffedPerPlayer();
		dragDistance = _getDragDistance();
		dragDistance2 = _getDragDistanceSquared();
		dragTicks = _getDragTicks();
		cuffPrisonerBarColor = _getCuffPrisonerBarColor();
		cuffGuardBarColor = _getCuffGuardBarColor();
		dropItemsWhenCuffed = _getDropItemsWhenCuffed();
		useQualityArmoryVehicles = _getUseQualityArmoryVehicles();
		drageeLeaveCommands = _getDrageeLeaveCommands();
		useBossbar = _useBossbar();
		isLoaded = true;
		stopInteractWhenCuffed = _stopInteractWhenCuffed();
		useVehiclesPlugin = _getUseVehiclesPlugin();
		uncuffSignInfo = _getUncuffSignInfo();
		stopPvpWhenCuffed = _getStopPvPWhenCuffed();
		useBstats = _useBstats();
		cuffCommandWithinWorld = _cuffCommandWithinSameWorld();
		cuffCommandRadiusSquared = _cuffCommandRadiusSquared();
		allowCuffNPCs = _allowCuffNPCs();
		cuffRightClickCmds = _getCuffRightClickCommands();
		dropTarget = _getDropTarget();
		allowCuffsWithoutTag = _getAllowCuffsWithoutTag();
		removeBindedArmor = _getRemoveBindedArmor();
	}

	public void reload() {
		loadSettings();
		CE.getIdenfitier().setCuffs(cuffItem);
	}

	private boolean isLoaded = false;

	public boolean isLoaded() {
		return isLoaded;
	}

	private Material cuffType = null;

	private Material _getCuffType() {
		String matName = config.getString("cuff-type");
		Material mat = Material.matchMaterial(matName);
		if (mat == null) {
			CE.getLogger().severe("Cuff type incorrect! Found " + matName);
		}
		return mat;
	}

	@Deprecated
	public Material getCuffType() {
		return cuffType;
	}

	public String getCuffTag() {
		return cuffTag;
	}

	private String cuffName;

	private String _getCuffName() {
		return Utils.color(config.getString("cuff-name"));
	}

	@Deprecated
	public String getCuffName() {
		return cuffName;
	}

	private ItemStack cuffItem;

	private ItemStack _getCuffItem() {
		ItemStack item = CE.getConfig().getItemStack("cuff-item");
		if (item == null) {
			CE.getLogger().info("Using legacy item");
			Material mat = _getCuffType();
			item = new ItemStack(mat == null ? Material.BLAZE_ROD : mat);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(_getCuffName());
			item.setItemMeta(meta);
		}
		return item;
	}

	public ItemStack getCuffItem() {
		return cuffItem;
	}

	public void setCuffItem(ItemStack item) {
		this.cuffItem = item;
		CE.getConfig().set("cuff-item", item);
		CE.getIdenfitier().setCuffs(item);
		CE.saveConfig();
	}

	private int maxCuffedPerPlayer;

	private int _getMaxCuffedPerPlayer() {
		return config.getInt("max-cuffed-per-person");
	}

	public int getMaxCuffedPerPlayer() {
		return maxCuffedPerPlayer;
	}

	private int dragDistance;

	private int _getDragDistance() {
		return config.getInt("drag-distance");
	}

	public int getDragDistance() {
		return dragDistance;
	}

	private int dragDistance2;

	private int _getDragDistanceSquared() {
		return _getDragDistance() * _getDragDistance();
	}

	public int getDragDistanceSquared() {
		return dragDistance2;
	}

	private int dragTicks;

	private int _getDragTicks() {
		return config.getInt("drag-ticks");
	}

	public int getDragTicks() {
		return dragTicks;
	}

	private String cuffPrisonerBarColor;

	private String _getCuffPrisonerBarColor() {
		return config.getString("cuffed-prisoner-bar-color");
	}

	public String getCuffPrisonerBarColor() {
		return cuffPrisonerBarColor;
	}

	private String cuffGuardBarColor;

	private String _getCuffGuardBarColor() {
		return config.getString("cuffed-guard-bar-color");
	}

	public String getCuffGuardBarColor() {
		return cuffGuardBarColor;
	}

	private boolean dropItemsWhenCuffed;

	private boolean _getDropItemsWhenCuffed() {
		return config.getBoolean("drop-items-upon-cuff");
	}

	public boolean getDropItemsWhenCuffed() {
		return dropItemsWhenCuffed;
	}

	private boolean useQualityArmoryVehicles;

	private boolean _getUseQualityArmoryVehicles() {
		return config.getBoolean("use-quality-armory-vehicles");
	}

	private boolean useVehiclesPlugin;

	private boolean _getUseVehiclesPlugin() {
		return config.getBoolean("use-vehicles-plugin");
	}

	public boolean getUseVehiclesPlugin() {
		return useVehiclesPlugin;
	}

	public boolean getUseQualityArmoryVehicles() {
		return useQualityArmoryVehicles;
	}

	private List<String> drageeLeaveCommands;

	private List<String> _getDrageeLeaveCommands() {
		return config.getStringList("cuffed-player-leave-commands");
	}

	public List<String> getDrageeLeaveCommands() {
		return drageeLeaveCommands;
	}

	private boolean useBossbar;

	private boolean _useBossbar() {
		return config.getBoolean("use-bossbar-when-cuffed", true);
	}

	public boolean useBossbar() {
		return useBossbar;
	}

	private boolean stopInteractWhenCuffed;

	private boolean _stopInteractWhenCuffed() {
		return config.getBoolean("stop-interact-when-cuffed", false);
	}

	public boolean stopInteractWhenCuffed() {
		return stopInteractWhenCuffed;
	}

	private UncuffSignInfo uncuffSignInfo;

	private UncuffSignInfo _getUncuffSignInfo() {
		return new UncuffSignInfo(CE, config.getStringList("uncuff-sign-commands"));
	}

	public UncuffSignInfo getUncuffSignInfo() {
		return uncuffSignInfo;
	}

	private boolean stopPvpWhenCuffed;

	private boolean _getStopPvPWhenCuffed() {
		return config.getBoolean("stop-pvp-when-cuffed", false);
	}

	public boolean getStopPvPWhenCuffed() {
		return stopPvpWhenCuffed;
	}

	private boolean useBstats;

	private boolean _useBstats() {
		return CE.getConfig().getBoolean("use-bstats", true);
	}

	public boolean useBstats() {
		return useBstats;
	}

	private boolean cuffCommandWithinWorld;

	private boolean _cuffCommandWithinSameWorld() {
		return config.getBoolean("cuff-command-limitations.only-within-same-world", false);
	}

	public boolean cuffCommandWithinSameWorld() {
		return cuffCommandWithinWorld;
	}

	private double cuffCommandRadiusSquared;

	private double _cuffCommandRadiusSquared() {
		double rad = config.getDouble("cuff-command-limitations.within-radius", -1.0D);
		if (rad < 0) {
			return rad;
		}
		return rad * rad;
	}

	public double cuffCommandRadiusSquared() {
		return cuffCommandRadiusSquared;
	}

	private boolean allowCuffNPCs;

	private boolean _allowCuffNPCs() {
		return config.getBoolean("allow-cuff-NPCs", false);
	}

	public boolean allowCuffNPCs() {
		return allowCuffNPCs;
	}

	private List<String> cuffRightClickCmds;

	private List<String> _getCuffRightClickCommands() {
		return config.getStringList("right-click-cuff-side-effect-commands");
	}

	public List<String> getCuffRightClickCommands() {
		return cuffRightClickCmds;
	}

	private DropTarget dropTarget;

	private DropTarget _getDropTarget() {
		String strVal = config.getString("drop-item-target", "GROUND");
		try {
			return DropTarget.valueOf(strVal.toUpperCase());
		} catch (IllegalArgumentException e) {
			CE.getLogger().warning("Unable to parse drop-item-target " + strVal + ". Using GROUND (default)");
			return DropTarget.GROUND;
		}
	}

	public DropTarget getDropTarget() {
		return dropTarget;
	}

	private boolean allowCuffsWithoutTag;

	private boolean _getAllowCuffsWithoutTag() {
		return config.getBoolean("allow-cuffs-without-tag", false);
	}

	public boolean allowCuffsWithoutTag() {
		return allowCuffsWithoutTag;
	}
	
	private boolean removeBindedArmor;

	private boolean _getRemoveBindedArmor() {
		return config.getBoolean("remove-binded-armor", false);
	}

	public boolean getRemoveBindedArmor() {
		return removeBindedArmor;
	}

}
