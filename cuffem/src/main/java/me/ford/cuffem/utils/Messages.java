package me.ford.cuffem.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.configuration.file.FileConfiguration;

import me.ford.cuffem.CuffEmPlugin;

public enum Messages {
	PLAYER_NOT_FOUND("player-not-found", "&cPlayer not found: &8{player}"),
	GAVE_PLAYER_CUFFS("gave-player-cuffs", "&6Gave cuffs to &8{player}"),
	RECEIVED_CUFFS("received-cuffs", "&cReceived a shiny pair of cuffs!"),
	LOG_PLAYER_GOT_CUFFS("log.player-got-cuffs", "&8{player}&6 got cuffs from &7{from}"),
	BEING_CUFFED("player-being-cuffed", "&6You have been cuffed by &8{player}"),
	CUFFING("player-cuffing-other", "&6You have cuffed &8{player}"),
	LOG_PLAYER_CUFFING_PLAYER("log.player-cuffing-other", "&8{player} &6 is cuffing &7{other}"),
	BEING_UNCUFFED("player-being-uncuffed", "&6The cuffs from &8{player}&6 have been lifted"),
	UNCUFFING("player-uncuffing-other", "&6The cuffs on &8{player}&6 have been lifted"),
	LOG_PLAYER_UNCUFFING_PLAYER("log.player-uncuffing-player", "&8{player}&6 uncuffed &7{other}"),
	ALREADY_CUFFING_TOO_MANY("player-already-dragging",
			"&6You are already cuffing &4{nr}&6 others, unable to cuff more"),
	PLAYER_NOT_CUFFED("player-not-cuffed", "&6The player &8{player}&6 is not cuffed by anyone!"),
	PLAYER_NOT_CUFFED_BY_YOU("player-not-cuffed-by-you", "&6The player &8{player}&6 is not cuffed by you!"),
	UNCUFFED_PLAYER("uncuffed-player", "&6Uncuffed &8{player}&6!"),
	CUFFED_INVENTORY("cuffed-cannot-interact", "&6You are cuffed and thus cannot use your inventory right now"),
	NOT_LOOKING_AT_SIGN("not-looking-at-sign", "You are not looking at a sign"),
	SIGN_ALREADY_MARKED("sign-already-marked", "This sign has already been marked as an uncuff sign"),
	NOT_DRAGGING_ANYONE("not-dragging-anyone",
			"You are currently not dragging anyone and thus cannot use the uncuff sign"),
	MARKED_SIGN_AS_UNCUFF_SIGN("sign-marked-for-uncuff", "The sign has successfully been marked as an uncuff sign"),
	CUFFED_CANNOT_PVP("cuffed-cannot-pvp", "&6You are currendly cuffed an thus cannot PvP right now"),
	SET_CUFF_ITEM("set-cuff-item", "Set the new cuff item to the item in your hand"),
	RELOADED("reloaded", "Reloaded the config"),
	PLAYER_IMMUNE("player-immune", "&6Player &8{player}&6 is immune to cuffing"),
	ALREADY_CUFFING("already-cuffing", "&cYou have already cuffed &8{player}"),
	CANNOT_CUFF_SELF("cannot-cuff-self", "&cYou cannot cuff yourself"),
	TARGET_IN_WRONG_WORLD("target-in-wrong-world", "&cYou cannot cuff somebody in a different world"),
	TARGET_TOO_FAR("target-too-far", "&cThe target player is too far away"),
	NO_PERM_FOR_CUFFS("not-allowed-to-cuff", "&cYou do not have permission to use cuffs right now"),
	ITEMS_DROPPED_DRAGEE("items-dropped-on-uncuff-dragee",
			"&cNot all items fit in your inventory so &8{nr}&c of them were dropped"),
	ITEMS_NOT_DROPPING_UPON_CUFF("items-not-dropping-upon-cuff", "&cItems are not set to drop upon cuffing"),
	DROPPER_STRAT_NO_STORE("dropper-strategy-no-store",
			"&cThe current dropper strategy does not allow storing of inventories"),
	DROPPER_NO_STORED_INVENTORY("dropper-no-stored-inventory",
			"&cThere does not seem to be an inventory for player &8{player}"),
	DROPPER_CLEARED_STORED_INVENTORY("dropper-cleared-stored-inventory",
			"&cCleared the stored inventory of &8{player}"),
	TARGET_DRAGGING_YOU("target-dragging-you", "&cYou cannot drag this player because they are dragging you!"),
	NO_ITEM_TO_SET("no-item-to-set", "&cYou do not have an item in your hand to set as a cuff item!"),
	;

	private static FileConfiguration file;
	private static final String messagesPath = "messages.";
	private final String path;
	private final String def;
	private String message;
	private static CuffEmPlugin CE;
	private static boolean initialized = false;

	/**
	 * Initializes Messages, including the file
	 * 
	 * @param plugin
	 */
	public static void init(CuffEmPlugin plugin) {
		CE = plugin;
		initialized = true;
		file = CE.getConfig();
		for (Messages m : Messages.values()) {
			m.updateMessage();
		}
	}

	/**
	 * @param path    path of message within file
	 * @param message default message
	 */
	private Messages(String path, String def) {
		this.path = messagesPath + path;
		this.def = def;
		this.message = def;
	}

	/**
	 * Updates the message from config on initialization
	 * Colors message;
	 */
	private void updateMessage() {
		message = c(file.getString(path, def));
	}

	/**
	 * @return coloured message
	 * @throws NotInitializedException if Messages have not yet been initialized
	 */
	public String get() {
		if (initialized) {
			return message;
		} else {
			throw new NotInitializedException();
		}
	}

	/**
	 * Get (coloured) message where a map is used to swap out {} tagged stuff
	 * 
	 * @param map map used to replace
	 * @return the coloured message
	 * @throws NotInitializedException if Messages have not yet been initialized
	 */
	public String get(Map<String, String> map) {
		if (initialized) {
			String msg = message;
			for (Entry<String, String> entry : map.entrySet()) {
				msg = msg.replaceAll(entry.getKey(), entry.getValue());
			}
			return c(msg);
		} else {
			throw new NotInitializedException();
		}
	}

	/**
	 * Parses array of string into map.
	 * In case of an odd number, the last string is ignored.
	 * 
	 * @param strings array to parse
	 * @return message with mappings
	 */
	public String get(String... strings) {
		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < strings.length; i += 2) {
			map.put(strings[i], strings[i + 1]);
		}
		return get(map);
	}

	/**
	 * Colours message
	 * 
	 * @param msg message to color
	 * @return coloured message
	 */
	public String c(String msg) {
		if (msg == null) {
			return "";
		}
		return Utils.color(msg);
	}

	public static class NotInitializedException extends RuntimeException {

		/**
		 * 
		 */
		private static final long serialVersionUID = 857735648108434141L;

		public NotInitializedException() {
			super("Messages need to be initialized with .init() before use!");
		}

	}

}
