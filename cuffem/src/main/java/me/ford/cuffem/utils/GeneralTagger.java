package me.ford.cuffem.utils;

import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataHolder;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.java.JavaPlugin;

public final class GeneralTagger {
	
	private GeneralTagger() {}
	
	public static ItemStack tag(JavaPlugin plugin, ItemStack item, String tagname, String tag) {
		ItemMeta meta = item.getItemMeta();
		PersistentDataContainer container = ((PersistentDataHolder) meta).getPersistentDataContainer();
		NamespacedKey key = new NamespacedKey(plugin, tagname);
		container.set(key, PersistentDataType.STRING, tag);
		item.setItemMeta(meta);
		return item;
	}
	
	public static boolean checkTag(JavaPlugin plugin, ItemStack item, String tagname, String tag) {
		ItemMeta meta = item.getItemMeta();
		PersistentDataContainer container = ((PersistentDataHolder) meta).getPersistentDataContainer();
		NamespacedKey key = new NamespacedKey(plugin, tagname);
		return container.has(key, PersistentDataType.STRING);
		
	}

}
