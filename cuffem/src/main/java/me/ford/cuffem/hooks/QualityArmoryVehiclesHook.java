package me.ford.cuffem.hooks;

import java.util.ConcurrentModificationException;
import java.util.Set;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.spigotmc.event.entity.EntityDismountEvent;

import me.ford.cuffem.CuffEmPlugin;
import me.zombie_striker.qg.exp.cars.VehicleEntity;
import me.zombie_striker.qg.exp.cars.api.QualityArmoryVehicles;
import me.zombie_striker.qg.exp.cars.api.events.PlayerEnterQAVehicleEvent;
import me.zombie_striker.qg.exp.cars.Main;

public class QualityArmoryVehiclesHook implements Listener {
	private final CuffEmPlugin CE;
	private final QAV_Version qavVersion;
			
	public QualityArmoryVehiclesHook(CuffEmPlugin plugin) {
		CE = plugin;
		Main pl = (Main) CE.getServer().getPluginManager().getPlugin("QualityArmoryVehicles");
		String version = pl.getDescription().getVersion();
		if (version.contains("1.0.1")) {
			qavVersion = QAV_Version.OLD;
		} else if (version.contains("1.0.2") ||
					version.contains("1.0.3") ||
					version.contains("1.0.4")){
			qavVersion = QAV_Version.NEW;
		} else {
			qavVersion = QAV_Version.UNDETERMINED;
			CE.getLogger().severe("Could not determine QualityArmoryVehicles version from " + version);
			CE.getLogger().severe("QualityArmoryVehicles support will essentially be non-existant");
		}
		
	}
	
	private void putInside(final VehicleEntity vehicle, final Player dragee) {
		CE.getServer().getScheduler().runTask(CE, new Runnable() {
			
			public void run() {
				QualityArmoryVehicles.addPlayerToCar(vehicle, dragee);
				try {
					vehicle.updateSeats();
				} catch (ConcurrentModificationException e) {
					// happens when prisoner tries to escape by spamming Shift to leave the vehicle
				}
			}
			
		});
	}
	
	@EventHandler
	public void onPlayerEnterQAVehicleEvent(PlayerEnterQAVehicleEvent event) {
		//CE.debug("ENTERING VEHICLE: " + event.getPlayer().getName());
		Player dragger = event.getPlayer();
		Set<Player> beingDragged = CE.getDragger().isDragging(dragger);
		if (beingDragged != null && !beingDragged.isEmpty()) {
			final VehicleEntity vehicle = event.getVehicle();
			for (final Player dragee : beingDragged) {
				if (!dragee.isInsideVehicle() || !QualityArmoryVehicles.isVehicle(dragee.getVehicle())) {
					// add these AFTER the initial one got in
					putInside(vehicle, dragee);
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerDismount(EntityDismountEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			Entity vehicle = event.getDismounted();
			CE.debug("EntityDismountEvent with " + player.getName() + " and " + vehicle);
			if (QualityArmoryVehicles.isVehicle(vehicle)) {
				VehicleEntity veh = getVehicle((ArmorStand) vehicle);
				Set<Player> beingDragged = CE.getDragger().isDragging(player);
				if (beingDragged != null && !beingDragged.isEmpty()) {
					// dragees leave as well
					for (Player dragee : beingDragged) {
						if (dragee.isInsideVehicle()) {
							dragee.leaveVehicle();
							CE.debug("Pushing out of vehicle:" + player.getName());
						}
					}
					veh.updateSeats();
				}
			}
			Player dragger = CE.getDragger().beingDraggedBy(player);
			if (dragger != null) {
				if (dragger.isInsideVehicle() && 
						(QualityArmoryVehicles.isVehicle(dragger.getVehicle()))) {
					VehicleEntity veh = QualityArmoryVehicles.getVehicleEntity((ArmorStand) dragger.getVehicle());
					veh.updateSeats();
					CE.debug("Tried to get out, but putting back into vehicle:" + player.getName());
					putInside(veh, player);
				}
			}
		}
	}
	
	private VehicleEntity getVehicle(ArmorStand vehicle) {
		if (qavVersion == QAV_Version.OLD) {
			return QualityArmoryVehicles.getVehicleEntity(vehicle);
		} else if (qavVersion == QAV_Version.NEW) {
			return QualityArmoryVehicles.getVehicleEntity((Entity) vehicle);
		}
		return null;
	}
	
	private enum QAV_Version {
		OLD, NEW, UNDETERMINED
	}

}
