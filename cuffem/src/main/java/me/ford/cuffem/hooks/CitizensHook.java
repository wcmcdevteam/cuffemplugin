package me.ford.cuffem.hooks;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import net.citizensnpcs.api.CitizensAPI;

public class CitizensHook {
    private static final ICitizensHook hook;
    static {
        Plugin plugin = Bukkit.getPluginManager().getPlugin("Citizens");
        if (plugin == null || !(plugin instanceof JavaPlugin) || !plugin.isEnabled()) {
            hook = new DummyCitizensHook();
        } else {
            hook = new RealCitizensHook();
        }
    }

    public static boolean isNPC(Player player) {
        return hook.isNPC(player);
    }

    private static interface ICitizensHook {
        boolean isNPC(Player player);
    }

    private static class RealCitizensHook implements ICitizensHook {

        private RealCitizensHook() {
            if (!CitizensAPI.getPlugin().isEnabled()) {
                throw new IllegalStateException("Citizens not enabled");
            }
        }

        @Override
        public boolean isNPC(Player player) {
            return CitizensAPI.getNPCRegistry().isNPC(player);
        }

    }

    private static class DummyCitizensHook implements ICitizensHook {

        @Override
        public boolean isNPC(Player player) {
            return false; // not installed -> not an NPC
        }

    }

}
