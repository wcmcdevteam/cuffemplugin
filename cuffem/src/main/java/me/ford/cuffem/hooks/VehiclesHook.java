package me.ford.cuffem.hooks;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.spigotmc.event.entity.EntityDismountEvent;

import es.pollitoyeye.vehicles.VehiclesMain;
import es.pollitoyeye.vehicles.events.VehicleEnterEvent;
import es.pollitoyeye.vehicles.events.VehicleExitEvent;
import es.pollitoyeye.vehicles.interfaces.Vehicle;
import es.pollitoyeye.vehicles.vehicle.Bike;
import es.pollitoyeye.vehicles.vehicle.Car;
import es.pollitoyeye.vehicles.vehicle.Plane;
import me.ford.cuffem.CuffEmPlugin;

public class VehiclesHook implements Listener {
    private final CuffEmPlugin CE;
    private final VehiclesMain vehicles;

    public VehiclesHook(CuffEmPlugin plugin) {
        CE = plugin;
        vehicles = JavaPlugin.getPlugin(VehiclesMain.class);
        CE.getServer().getPluginManager().registerEvents(this, plugin);
        CE.getServer().getScheduler().runTaskTimer(CE, () -> {
            long oldStamp = System.currentTimeMillis() - 1000L; // more than second old gets removed
            stampedOutGetters.values().removeIf((s) -> s < oldStamp);
        }, 200L, 200L);
    }

    @EventHandler
    public void onPlayerEnterVehicle(VehicleEnterEvent event) {
        CE.debug("ENTERING VEHICLE: " + event.getPlayer().getName());
        Player dragger = event.getPlayer();
        // This plugin is SO FUCKING STUPID
        // You can't even find out which vehicle an armor stand represents!
        ArmorStand as = event.getMainArmorStand();
        // since a player "has" a vehicle only after they've got in it, will need to
        // delay by a tick
        CE.getServer().getScheduler().runTask(CE, () -> processEnterEvent(dragger, as));
    }

    private void processEnterEvent(Player dragger, ArmorStand as) {
        Vehicle vehicle = vehicles.playerVehicles.get(dragger);
        if (vehicle == null) {
            CE.debug("Player does not have vehicle:" + dragger.getName());
            return;
        }
        if (!vehicle.getMainStand().equals(as)) {
            CE.debug("No idea which vehicle is being entered...");
            return;
        }
        Set<Player> beingDragged = CE.getDragger().isDragging(dragger);
        if (beingDragged != null && !beingDragged.isEmpty()) {
            Player dragee = beingDragged.iterator().next();
            putInside(vehicle, dragee);
            if (beingDragged.size() > 1) {
                CE.getLogger()
                        .warning("Vehicles never have more than two slots." + " The current player is dragging "
                                + beingDragged.size() + " players."
                                + " Will only attempt to add one of the dragged players into the vehicle.");
            }
        }
    }

    private void putInside(Vehicle vehicle, Player dragee) {
        putInside(vehicle, dragee, 1L);
    }

    private void putInside(Vehicle vehicle, Player dragee, long delay) {
        CE.getServer().getScheduler().runTaskLater(CE, () -> {
            CE.debug("Attempting to put " + dragee.getName() + " into " + vehicle + " " + delay + " ticks later");
            // the idiotic vehicle plugin design forces this idiotic behaviour below
            if (vehicle instanceof Car) {
                Car car = (Car) vehicle;
                car.getSecondSeatStand().addPassenger(dragee); // ASSUME it's empty ...
            } else if (vehicle instanceof Bike) {
                Bike bike = (Bike) vehicle;
                bike.getSecondSeatStand().addPassenger(dragee); // ASSUME it's empty
            } else if (vehicle instanceof Plane) {
                Plane plane = (Plane) vehicle;
                plane.getSecondSeatStand().addPassenger(dragee); // ASSUME it's empty
            } else {
                CE.getLogger().warning("Trying to put a dragged player in a vehicle with no second slot");
            }
        }, delay);
    }

    @EventHandler
    public void onDrageeDismount(EntityDismountEvent event) {
        if (!(event.getDismounted() instanceof ArmorStand)) {
            return; // not a vehicle
        }
        Entity entity = event.getEntity();
        if (!(entity instanceof Player)) {
            return;
        }
        Player dismountingPlayer = (Player) entity;
        CE.debug("LEAVING VEHICLE: " + dismountingPlayer.getName());
        Player dragger = CE.getDragger().beingDraggedBy(dismountingPlayer);
        if (dragger != null) {
            Vehicle vehicle = vehicles.getPlayerVehicle(dragger);
            if (vehicle == null) {
                return; // not of interest (dragger not in vehicle)
            }
            Long stamp = stampedOutGetters.get(dismountingPlayer.getUniqueId());
            if (stamp != null && stamp > System.currentTimeMillis() - 50L) { // allowing forcing players out
                CE.debug("Leaving vehicle allowed since dragger left: " + dismountingPlayer.getName());
                stampedOutGetters.remove(dismountingPlayer.getUniqueId());
                return;
            }
            CE.debug("Leaving vehicle cancelled for dragee: " + dismountingPlayer.getName());
            // still need to put back on...
            putInside(vehicle, dismountingPlayer, 2L);
        }
    }

    private final Map<UUID, Long> stampedOutGetters = new HashMap<>(); // potential memory leak

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onCufferDismount(VehicleExitEvent event) {
        Player player = event.getPlayer();
        Set<Player> dragees = CE.getDragger().isDragging(player);
        if (dragees != null && !dragees.isEmpty()) {
            CE.debug("Forcing dragees of " + event.getPlayer().getName() + " to leave vehicle");
            for (Player dragee : dragees) {
                if (dragee.isInsideVehicle()) {
                    stampedOutGetters.put(dragee.getUniqueId(), System.currentTimeMillis());
                    dragee.leaveVehicle();
                }
            }
        }
    }

}
