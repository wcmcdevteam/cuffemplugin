package me.ford.cuffem;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import me.ford.cuffem.utils.Messages;

public class Dragger {
	private final CuffEmPlugin CE;
	// since I'm dragging entities then storaing players could be (somewhat) justified
	private Map<Player, Set<Player>> draggers = new HashMap<Player, Set<Player>>();
	private Map<Player, BossBar> draggerBars = new HashMap<Player, BossBar>(); // key for this is dragee as there is only 1 bar per dragee
	private Map<Player, Player> dragees = new HashMap<Player, Player>();
	private Map<Player, BossBar> drageeBars = new HashMap<Player, BossBar>();
	
	public Dragger(CuffEmPlugin plugin) {
		CE = plugin;
	}
	
	public void startDragging(Player dragger, Player dragee) {
		if (!draggers.containsKey(dragger)) {
			draggers.put(dragger, new HashSet<Player>());
		}
		dragees.put(dragee, dragger);
		draggers.get(dragger).add(dragee);
		String drageeMsg = Messages.BEING_CUFFED.get("\\{player\\}", dragger.getName());
		dragee.sendMessage(drageeMsg);
		BarColor color = BarColor.valueOf(CE.getSettings().getCuffPrisonerBarColor().toUpperCase());
		if (color == null) {
			color = BarColor.RED;
		}
		if (CE.getSettings().useBossbar()) {
			BossBar drageeBar = dragger.getServer().createBossBar(drageeMsg, 
														color, BarStyle.SEGMENTED_20, BarFlag.DARKEN_SKY);
			drageeBar.setVisible(true);
			drageeBar.setProgress(1.0);
			drageeBar.addPlayer(dragee);
			drageeBars.put(dragee, drageeBar);
		}
		String draggerMsg = Messages.CUFFING.get("\\{player\\}", dragee.getName());
		dragger.sendMessage(draggerMsg);
		color = BarColor.valueOf(CE.getSettings().getCuffGuardBarColor().toUpperCase());
		if (color == null) {
			color = BarColor.GREEN;
		}
		if (CE.getSettings().useBossbar()) {
			BossBar draggerBar = dragger.getServer().createBossBar(draggerMsg, 
														color, BarStyle.SEGMENTED_20, BarFlag.DARKEN_SKY);
			draggerBar.setVisible(true);
			draggerBar.setProgress(1.0);
			draggerBar.addPlayer(dragger);
			draggerBars.put(dragee, draggerBar);
		}
		if (CE.getSettings().getDropItemsWhenCuffed()) {
			CE.getDropper().dropInventory(dragee);
			dragee.setCanPickupItems(false);
		}
		CE.getLogger().info(Messages.LOG_PLAYER_CUFFING_PLAYER.get("\\{player\\}", dragger.getName(), "\\{other\\}", dragee.getName()));
	}
	
	public boolean isBeingDragged(Player player) {
		return beingDraggedBy(player) != null;
	}
	
	public Player beingDraggedBy(Player player) {
		return dragees.get(player); // dragger
	}
	
	public Set<Player> isDragging(Player player){
		return draggers.get(player); // dragees
	}

	public void stopDragging(Player player) throws NotBeingDraggedException {
		stopDragging(player, true);
	}

	public boolean isDraggingSpecific(Player dragger, Player dragee) {
		if (!isBeingDragged(dragee)) {
			return false;
		}
		Set<Player> draggerDragged = isDragging(dragger);
		return draggerDragged != null && draggerDragged.contains(dragee);
	}

	public void stopDragging(Player player, boolean getInventoryBack) throws NotBeingDraggedException {
		if (isBeingDragged(player)) {
			Player dragger = dragees.get(player);
			draggers.remove(dragger);
			dragees.remove(player);
			if (CE.getSettings().useBossbar()) {
				BossBar draggerBar = draggerBars.get(player); // dragee as key as dragger might have multiple
				BossBar drageeBar = drageeBars.get(player);
				draggerBar.removeAll();
				drageeBar.removeAll();
				draggerBars.remove(player);
				drageeBars.remove(player);
			}
			player.sendMessage(Messages.BEING_UNCUFFED.get("\\{player\\}", dragger.getName()));
			dragger.sendMessage(Messages.UNCUFFING.get("\\{player\\}", player.getName()));
			CE.getLogger().info(Messages.LOG_PLAYER_UNCUFFING_PLAYER.get("\\{player\\}", dragger.getName(), "\\{other\\}", player.getName()));
			if (CE.getSettings().getDropItemsWhenCuffed()) {
				player.setCanPickupItems(true);
				if (getInventoryBack && CE.getDropper().canReturnInventory() && CE.getDropper().hasSavedInventoryFor(player)) {
					CE.getDropper().returnInventory(player);
				}
			}
		} else {
			throw new NotBeingDraggedException();
		}
	}
	
	public int numberOfDragees(Player player) {
		if (draggers.containsKey(player)) {
			return draggers.get(player).size();
		} else {
			return 0;
		}
	}
	
	public static class NotBeingDraggedException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 8345959800950874673L;
		
		public NotBeingDraggedException() {
			super("This player is not being dragged!");
		}
		
	}

}
