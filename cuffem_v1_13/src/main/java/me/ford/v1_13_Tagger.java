package me.ford;

import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import net.minecraft.server.v1_13_R2.NBTTagCompound;

/**
 * Hello world!
 *
 */
public class v1_13_Tagger 
{
	public static ItemStack tag(ItemStack item, String tagname, String tag) {
		net.minecraft.server.v1_13_R2.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
		NBTTagCompound c = nmsItem.getTag();
		c.setString(tagname, tag);
		nmsItem.setTag(c);
		return CraftItemStack.asBukkitCopy(nmsItem);
	}
	
	public static boolean checkTag(ItemStack item, String tagname, String tag) {
		net.minecraft.server.v1_13_R2.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
		if (nmsItem.hasTag()) {
			//CE.debug("has \"tag\"");
			NBTTagCompound c = nmsItem.getTag();
			String key = c.getString(tagname);
			if (key != null && key.equals(tag)) {
				//CE.debug("Has correct tag with correct value");
				return true;
			}
		}
		return false;
	}
}
